AHK Spotify Toolbar
==============

This program is a simplistic toolbar for desktop spotify that sits in the Windows taskbar. It uses autohotkey to grab the window title and provide simple controls. 
It doesn't use the spotify web api, and so is simpler and quicker, but also lacks features and is a bit hacky. 
