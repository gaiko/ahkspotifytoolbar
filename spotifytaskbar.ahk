;#NoTrayIcon
gui, -dpiscale

scaleFactor := 1.25 ;(A_ScreenDPI/96), or the screenDPI of the taskbar monitor/96

spotifyExePath := "" ;fill me in
barXPos := round(A_ScreenWidth - 700)
barWidth := round(300 * scaleFactor)

barHeight := round(40 * scaleFactor)
smallButtonSize := round(20 * scaleFactor)
largeButtonSize := round(26 * scaleFactor)
smallButtonYGap := round(10 * scaleFactor)
largeButtonYGap := round(7 * scaleFactor)
buttonXGap := round(4 * scaleFactor)
textYGap := round(3 * scaleFactor)
artistTextGap := round(2 * scaleFactor)
fontSize := round(8 * scaleFactor)
startLetter = 1
startScrollingDelay = 10

Gui, -Caption
Gui, Font, % "cFFFFFF w600 s"fontSize
Gui, Color, 000000 ;lol iunno how this works

Gui, Add, Picture, % "gBack y"smallButtonYGap " w"smallButtonSize " h"smallButtonSize, arrow-left-48.ico
Gui, Add, Picture, % "gPlay xm" (smallButtonSize + buttonXGap) " y"largeButtonYGap " w"largeButtonSize " h"largeButtonSize " vPlayPauseIcon", play-48.ico
Gui, Add, Picture, % "gForward xm" (smallButtonSize + largeButtonSize + buttonXGap + buttonXGap) " y"smallButtonYGap " w"smallButtonSize " h"smallButtonSize, arrow-right-48.ico

Gui, Add, Text, % "xp+" (smallButtonSize + buttonXGap) " vTitleText y"textYGap " w2000", Song Title
Gui, Add, Text, % "vArtistText y+"artistTextGap " w2000", Song Artist
Gui, Add, Text, % "gToggleSpotify xp y0 h"barHeight " w"barWidth ;clickable cover area

Gui, Show, % "x"barXPos " y0 w"barWidth " h" barHeight, spottool

Menu, ContextMenu, Add, Open Spotify, ToggleSpotify
Menu, ContextMenu, Add
Menu, ContextMenu, Add, Play/Pause, Play
Menu, ContextMenu, Add, Back, Back
Menu, ContextMenu, Add, Forward, Forward
Menu, ContextMenu, Add
Menu, ContextMenu, Add, Settings, Settings
Menu, ContextMenu, Add, About, About
Menu, ContextMenu, Add, Restart, Restart
Menu, ContextMenu, Add, Exit, GuiClose

Process, Exist
WinGet, hw_gui, ID, ahk_class AutoHotkeyGUI ahk_pid %ErrorLevel%
hw_tray := DllCall( "FindWindowEx", "uint", 0, "uint", 0, "str", "Shell_TrayWnd", "uint", 0 )
DllCall( "SetParent", "uint", hw_gui, "uint", hw_tray )

SetTimer, SetInfo, 500

Return

SetInfo:
	;GuiControlGet, pos, Pos, TitleText
	;newx := posx - 10
	;GuiControl, Move, TitleText, % "x"newx

	if (startLetter = 1) {
		if (startScrollingDelay = 0) {
			startLetter := 2
		} else {
			startScrollingDelay := startScrollingDelay - 1
		}
	} else {
		startLetter := startLetter + 1
	}




	;If the mindow is not minimised, it should contain a text box.
	;We have to avoid the window controls, which have their own window
	WinGet, spotifyWindowId, ID, ahk_exe Spotify.exe, Chrome Legacy Window
	;If we didn't get the window, it's minimised, so it's fine to match without the text
	if (spotifyWindowId = "") {
		WinGet, spotifyWindowId, ID, ahk_exe Spotify.exe
	}
	WinGetTitle, spotifyWindowTitle, ahk_id %spotifyWindowId%

	FirstBreak := InStr(spotifyWindowTitle, " - ")
	;there's no breaks on '-', just put it all in the bottom Text
	;other wise split it for, hopefully, artist and song
	If (FirstBreak == 0) {
		Artist := spotifyWindowTitle
		Song := ""
	} Else {
		Artist := SubStr(spotifyWindowTitle, 1, FirstBreak)
		Song := SubStr(spotifyWindowTitle, FirstBreak+3)
	}

	;skip if there's an advertisement and stop
	if (SubStr(Artist, 1, 7) = "Adverti") {
			goto Forward
			Return
	}

	;escape any ampersands, for the text controls
	Artist := StrReplace(Artist, "&", "&&")
	Song := StrReplace(Song, "&", "&&")

	;show pause button when nothing's playing, so the window title's 'spotify.
	;The title will sometimes be 'spotify free'
	;i guess this won't work when there's only text in the artist that happens to start with 'spotify'
	;so a podcast?
	if (SubStr(Artist, 1, 7) = "spotify" or (Artist = "")) {
		Artist := "-- Spotify --"
		GuiControl,, PlayPauseIcon, play-48.ico
	} else {
		GuiControl,, PlayPauseIcon, pause-48.ico
	}

	;Artist := "The quick brown fox jumps over the lazy dog"
	;Song := "Sphinx of black quartz, judge my vow"
	CurrentSong := SubStr(Song, startLetter)
	CurrentArtist := SubStr(Artist, startLetter)
	if (CurrentArtist = "" and CurrentSong = "") {
		startLetter := 1
		startScrollingDelay := 10
	}
	GuiControl,,TitleText, %CurrentSong%
	GuiControl,,ArtistText, %CurrentArtist%
Return

Back:
	Send {Media_Prev}
Return

Play:
	Send {Media_Play_Pause}
Return

Forward:
	Send {Media_Next}
Return

^+PgDn::
ToggleSpotify:
	;if spotify's open try and toggle it, else run it
	if (spotifyWindowId != "") {
	;if WinExist(ahk_id %spotifyWindowId%) {
		;if minimised, restore it
		WinGet, SpotifyMax, MinMax, ahk_id %spotifyWindowId%
		if (SpotifyMax == -1) {
			MsgBox, minimised
			WinRestore, ahk_id %spotifyWindowId%
		;if not minimised and not active, activate it
		} else if (not WinActive("ahk_id"%spotifyWindowId%)) {
			MsgBox, inactive
			WinActivate, ahk_id %spotifyWindowId%
		;if not minimised and is active, minimise it
		} else {
			MsgBox, active
			WinMinimize, ahk_id %spotifyWindowId%
		}
	} else {
		MsgBox, launching
		run %spotifyExePath%
	}
Return

GuiContextMenu:
	Menu, ContextMenu, Show
Return

Settings:
	Gui, Settings:New, ,Settings
	Gui, Settings:Add, Text, ,arostinein
	Gui, Settings:Show, x300 y300 w100 h50
Return

About:
Return

Restart:
	Reload
Return

GuiClose:
ExitApp

^+Insert::Send {Media_Play_Pause}
^+Delete::Send {Volume_Mute}
^+Home::Send {Media_Prev}
^+End::Send {Media_Next}
^+PgUp::Send {Volume_Up 2}
;^+PgDn::Send {Volume_Down 2}